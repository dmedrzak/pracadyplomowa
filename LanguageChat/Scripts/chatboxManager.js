﻿var chatHub = $.connection.chatHub;
$(document).ready(function () {
        var chatHub = $.connection.chatHub;
        registerClientMethods(chatHub);
        chatHub.client.sendPrivateMessage = function (fromUserId,userId, name, message) {
            var ctrId = 'private_' + userId;
            if (document.getElementById(fromUserId) !== null)
            {
                $('#' + userId).removeClass('btn-primary').addClass('btn-success');
            }
            $('#' + ctrId).append('<li><strong>' + htmlEncode(name)
                + '</strong>: ' + htmlEncode(message) + '</li>');           
        };
        $('#message').focus();
    $('#message').bind()
        $.connection.hub.start().done(function () {
            chatHub.server.connect();
            $('#sendmessage').click(function () {
                chatHub.server.sendPrivateMessage($('#toUserId').val(), $('#message').val());
                $('#' + $('#toUserId').val()).removeClass('btn-success').addClass('btn-primary');
                $('#message').val('').focus();
            });

            $('#message').bind("enterKey", function (e) {
                chatHub.server.sendPrivateMessage($('#toUserId').val(), $('#message').val());
                $('#' + $('#toUserId').val()).removeClass('btn-success').addClass('btn-primary');
                $('#message').val('').focus();
            });
            $('#message').keyup(function (e) {
                if (e.keyCode == 13) {
                    $(this).trigger("enterKey");
                }
            });
        });

});

var scrolMsg = (function scrollMessages(msg) {
    $('.chat_box_Messages').scrollTop($('ul li').last().position().top + $('ul li').last().height());
    return scrollMessages; //return the function itself to reference
});



function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}

function registerClientMethods(chatHub) {
    // Calls when user successfully logged in
    chatHub.client.onConnected = function (id, userName, allUsers) {
        // Add All Users
        for (i = 0; i < allUsers.length; i++) {
            AddUser(allUsers[i].Id, allUsers[i].UserName);
        }
    }
}


function AddUser(id, name) {
    var code = "";
    code = $('<button type="button" style="margin-top:5px" id="' + id +
        '" class="btn btn-primary btn-sm btn-block" > <i class=\"fa fa-user\"></i> ' + name + '</button>');
    $(code).dblclick(function () {
        var id = $(this).attr('id');
        $('#toUserId').val(id);
        $('#' + id).removeClass('btn-success').addClass('btn-primary');
        OpenPrivateChatWindow(id, name);
    });
    $("#chat_box_Users").append(code);
}


function OpenPrivateChatWindow(id, userName) {
    var ctrId = 'private_' + id;
    createPrivateChatWindow(id, ctrId, userName);
}

function createPrivateChatWindow(id,ctrId, userName) {
    $("#discussion").empty();
    $("#discussion").children().hide();
    if (document.getElementById(ctrId) !== null)
    {
        $('#' + ctrId).show();
    } else
    {
        $("#discussion").append("<ul id=\"" + ctrId + "\"></ul>");
    }
    var FromUserID = $('#CurrentUser').val();
    var ToUserID = id;
    chatHub.server.requestLastMessage(FromUserID, ToUserID);
}

chatHub.client.onNewUserConnected = function (id, name) {
    console.log("onNewUserConnected function");
    AddUser(id, name);
}


chatHub.client.onUserDisconnected = function (id, userName) {
    $('#' + id).remove();
}



chatHub.client.GetLastMessages = function (TouserID, CurrentChatMessages) {
    var ctrId = 'private_' + TouserID;
    var AllmsgHtml = "";
    for (i = 0; i < CurrentChatMessages.length; i++) {

        AllmsgHtml += '<li><strong>' + htmlEncode(CurrentChatMessages[i].FromUserName)
            + '</strong>: ' + htmlEncode(CurrentChatMessages[i].Message) + '</li>'
    }
    $('#' + ctrId).prepend(AllmsgHtml);
    scrolMsg();
}
