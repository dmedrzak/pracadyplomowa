﻿using LanguageChat.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using LanguageChat.Models.Chat;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Security;
using LanguageChat.Controllers;

namespace LanguageChat.Hubs
{

    [Authorize]
    [HubName("chatHub")]
    public class ChatHub : Hub
    {
        ApplicationDbContext _dbContext = new ApplicationDbContext();
        public void Connect()
        {
            var name = Context.User.Identity.Name;
            var userId = Context.User.Identity.GetUserId();
            var usersWhomMessageWithMe = GetUsersMessages(userId).Select(x=> new {x.Id,x.UserName });
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users
                    .Include(u => u.Connections)
                    .SingleOrDefault(u => u.UserName == name);
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        UserName = name,
                        Connections = new List<Models.Connection>()
                    };
                    db.Users.Add(user);
                }
                user.Connections.Add(new Models.Connection
                {
                    ConnectionID = Context.ConnectionId,
                    UserAgent = Context.Request.Headers["User-Agent"],
                    Connected = true
                });
                db.SaveChanges();
            }
            Clients.Caller.onConnected(userId, name, usersWhomMessageWithMe);
        }

        public void AddNewUserMessage(string toUser)
        {
            Task.WaitAll(Task.Delay(5000));
            bool ifChatExists = false;  
            var toUserName = _dbContext.Users.Where(x => x.Id == toUser).FirstOrDefault();
            var tousername = toUserName.UserName;
            var name = Context.User.Identity.Name;
            var userId = Context.User.Identity.GetUserId();
            var messagesWithUsers = GetUsersMessages(userId);
            foreach (var item in messagesWithUsers)
            {
                if(item.Id == toUser)
                {
                    ifChatExists = true;
                }
            }
            var connectedUsers = _dbContext.Connections.Where(x => x.Connected == true).ToList();
            var ToUsers = connectedUsers.Where(x => x.AppUser.Id == toUser).ToList();
            var FromUsers = connectedUsers.Where(x => x.AppUser.Id == userId).ToList();
            if (FromUsers.Count != 0 && ToUsers.Count() != 0 && ifChatExists == false)
            {
                foreach (var ToUser in ToUsers)
                {
                    Clients.Client(ToUser.ConnectionID).onNewUserConnected(userId, name);
                }
                foreach (var FromUser in FromUsers)
                {
                    Clients.Client(FromUser.ConnectionID).onNewUserConnected(toUser, tousername);
                }
            }
        }


        public void SendPrivateMessage(string toUserId, string message)
        {
            var connectedUsers = _dbContext.Connections.Where(x => x.Connected == true).ToList();
            var users = _dbContext.Users.ToList();
            try
            {
                string fromconnectionid = Context.ConnectionId;
                var fromUserId = Context.Request.User.Identity.GetUserId();
                var ToUsers = connectedUsers.Where(x => x.AppUser.Id == toUserId).ToList();
                var FromUsers = connectedUsers.Where(x => x.AppUser.Id == fromUserId).ToList();
                var fromUserName = Context.User.Identity.Name;
                if (FromUsers.Count != 0 && ToUsers.Count() != 0)
                {
                    foreach (var ToUser in ToUsers)
                    {
                        Clients.Client(ToUser.ConnectionID).sendPrivateMessage(fromUserId, fromUserId, fromUserName, message);
                    }
                    foreach (var FromUser in FromUsers)
                    {
                        Clients.Client(FromUser.ConnectionID).sendPrivateMessage(null,toUserId, fromUserName, message);
                    }                    
                    MessageDetail _MessageDeail = new MessageDetail { FromUserID = fromUserId,
                                                                        FromUserName = FromUsers[0].AppUser.UserName,
                                                                        ToUserID = toUserId,
                                                                        ToUserName = ToUsers[0].AppUser.UserName,
                                                                        Message = message,
                                                                        MessageDate = DateTime.Now };
                    AddMessageinCache(_MessageDeail);
                }
            }
            catch { }
        }

        public void SendChatMessage(string who, string message)
        {
            var name = Context.User.Identity.Name;
            string userid = Context.User.Identity.GetUserId();            
            using (var db = new ApplicationDbContext())
            {
                var fromUser = db.Users.Where(x => x.Id == userid).FirstOrDefault();
                var user = db.Users.Find(who);
                if (user == null)
                {
                    Clients.Caller.showErrorMessage("Could not find that user.");
                }
                else
                {
                    db.Entry(user)
                        .Collection(u => u.Connections)
                        .Query()
                        .Where(c => c.Connected == true)
                        .Load();

                    if (user.Connections == null)
                    {
                        Clients.Caller.showErrorMessage("The user is no longer connected.");
                    }
                    else
                    {
                        foreach (var connection in user.Connections)
                        {
                            Clients.Client(connection.ConnectionID)
                                .addChatMessage(name + ": " + message);
                        }
                        Clients.Caller.addChatMessage(name + ": " + message);
                    }
                }

            }
        }


        public override Task OnDisconnected(bool stopCalled = true)
        {           
            using (var db = new ApplicationDbContext())
            {
                var connection = db.Connections.Find(Context.ConnectionId);
                db.Connections.Remove(connection);
                db.SaveChanges();
            }
            return base.OnDisconnected(stopCalled);
        }


        public void RequestLastMessage(string FromUserID, string ToUserID)
        {

            var _fromUserId = _dbContext.Users.Where(x => x.UserName == FromUserID).FirstOrDefault();
            var currentmessages = _dbContext.MessageDetails.ToList();
            List<MessageDetail> CurrentChatMessages = (from u in currentmessages where
                                                       ((u.FromUserID == _fromUserId.Id && u.ToUserID == ToUserID)
                                                       || (u.FromUserID == ToUserID && u.ToUserID == _fromUserId.Id))
                                                       select u).ToList();
            Clients.Caller.GetLastMessages(ToUserID, CurrentChatMessages);
        }


        private void AddMessageinCache(MessageDetail _MessageDetail)
        {
            _dbContext.MessageDetails.Add(_MessageDetail);
            _dbContext.SaveChanges();
        }


        private List<ApplicationUser> GetUsersMessages(string fromUserId)
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            var currentmessages = _dbContext.MessageDetails.Where(x => x.FromUserID == fromUserId || x.ToUserID == fromUserId).ToList();
            foreach (var item in currentmessages)
            {
                if (item.FromUserID == fromUserId)
                {
                    var usrDetail = _dbContext.Users.Where(x => x.Id == item.ToUserID).FirstOrDefault();
                    if (!users.Contains(usrDetail))
                    {
                        users.Add(usrDetail);
                    }
                }
                else
                {
                    var usrDetail = _dbContext.Users.Where(x => x.Id == item.FromUserID).SingleOrDefault();
                    if (!users.Contains(usrDetail))
                    {
                        users.Add(usrDetail);
                    }
                }
            }
            return users;
        }

        private List<UserDetail> GetFriends(string userId)
        {
            List<UserDetail> myFriends = new List<UserDetail>();
            var users = _dbContext.UserDetails.ToList();
            var friendList = _dbContext.FriendLists.Where(x => x.FriendOne == userId || x.FriendSecond == userId).Where(y => y.FriendRequestOne == true && y.FriendRequestSecond == true).ToList();
            foreach (var item in friendList)
            {
                if(item.FriendOne == userId)
                {
                    var usr = users.Where(x => x.Id == item.FriendSecond).SingleOrDefault();
                    if(!myFriends.Contains(usr))
                    {
                        myFriends.Add(usr);
                    }
                }else
                {
                    var usr = users.Where(x => x.Id == item.FriendOne).SingleOrDefault();
                    if (!myFriends.Contains(usr))
                    {
                        myFriends.Add(usr);
                    }
                }
            }

            return myFriends;
        }


        private List<UserDetail> GetOnlineFriends(string userId)
        {
            List<UserDetail> listOfFriends = GetFriends(userId);
            var connected = _dbContext.Connections.ToList();
            var onlineFriends = listOfFriends.Where(x => connected.Any(y => y.AppUser.Id == x.Id && y.Connected == true)).ToList();

            return onlineFriends;
        }
    }
}