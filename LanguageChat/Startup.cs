﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LanguageChat.Startup))]
namespace LanguageChat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
