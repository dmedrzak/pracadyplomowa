﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LanguageChat.Models
{
    public class LanguageViewModel
    {
        public int LanguageId { get; set; }
        public List<Language> LanguageList { get; set; }
    }
}