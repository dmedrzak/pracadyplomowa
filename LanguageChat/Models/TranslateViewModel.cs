﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LanguageChat.Models
{
    public class TranslateViewModel
    {
        public int Id { get; set; }
        public string textToTranslate { get; set; }
        public string translatedText { get; set; }
        public string fromLang { get; set; }
        public string toLang { get; set; }
        public List<Language> LanguageList { get; set; }

    }
}