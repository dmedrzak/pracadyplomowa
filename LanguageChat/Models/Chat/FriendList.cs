﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LanguageChat.Models.Chat
{
    public class FriendList
    {
        [Key]
        public int FriendListId { get; set; }
        public string FriendOne { get; set; }
        public string FriendSecond { get; set; }
        public bool FriendRequestOne { get; set; }
        public bool FriendRequestSecond { get; set; }
        public DateTime RequestDate { get; set; }
    }
}