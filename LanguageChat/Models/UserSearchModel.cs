﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LanguageChat.Models
{
    public class UserSearchModel
    {
        public string Id { get; set; }
        public int? AgeFrom { get; set; }
        public int? AgeTo { get; set; }
        public bool Male { get; set; }
        public bool Female { get; set; }
        public string Country { get; set; }
        public int[] LanguageId { get; set; }
        public List<Language> LanguageList { get; set; }
    }
}