﻿using LanguageChat.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LanguageChat.Models
{
    public class UserDetail
    {
        public UserDetail()
        {
            this.UserDetailLanguages = new HashSet<UserDetailLanguage>();
        }
        [Key, ForeignKey("User")]
        public string Id { get; set; }
        public DateTime Birthday { get; set; }
        public string Sex { get; set; }
        public string Country { get; set; }
        public string About { get; set; }
        public double Vote { get; set; }
        [NotMapped]
        public int Age { get { return DateTime.Now.Year - Birthday.Year; } }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<UserDetailLanguage> UserDetailLanguages { get; set; }

    }
    public class UserDetailLanguage
    {
        public Int32 Id { get; set; }

        public virtual UserDetail UserDetail { get; set; }
        public string UserDetailId { get; set; }
        public virtual Language Language { get; set; }
        public Int32 LanguageId { get; set; }
        public Boolean IsKnown { get; set; }
        public static implicit operator List<object>(UserDetailLanguage v)
        {
            throw new NotImplementedException();
        }
    }
    public class Language
    {
        public Language()
        {
            this.UserDetailLanguages = new HashSet<UserDetailLanguage>();
        }
        public int Id { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserDetailLanguage> UserDetailLanguages { get; set; }
    }

}