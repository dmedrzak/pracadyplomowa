﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LanguageChat.Models
{
    public class Votes
    {
        public int Id { get; set; }
        public string FromUser { get; set; }
        public string ToUser { get; set; }
        public float Vote { get; set; }
    }
}