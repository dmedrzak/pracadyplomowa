﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using LanguageChat.Models;
using static LanguageChat.Models.Connection;
using System.Collections.Generic;
using LanguageChat.Models.Chat;

namespace LanguageChat.Models
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
        public byte[] UserPhoto { get; set; }

        public virtual ICollection<Connection> Connections { get; set; }
        public virtual UserDetail UserDetails { get; set; }
        
    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public virtual DbSet<Votes> Votes { get; set; }
        public virtual DbSet<FriendList> FriendLists { get; set; }
        public virtual DbSet<MessageDetail> MessageDetails { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<UserDetail> UserDetails { get; set; }
        public virtual DbSet<UserDetailLanguage> UserDetailLanguages { get; set; }
        public virtual DbSet<Connection> Connections { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }

}