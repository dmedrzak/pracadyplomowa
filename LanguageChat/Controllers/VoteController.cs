﻿using LanguageChat.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LanguageChat.Controllers
{
    public class VoteController : Controller
    {
        private ApplicationDbContext _dbContext = new ApplicationDbContext();
        // GET: Vote
        private string toUserId;

        public ActionResult SetUserId(string id)
        {
            toUserId = id;
            return RedirectToAction("Index","Vote",null);
        }

        public ActionResult Index(string id)
        {
            toUserId = id;
            var userId = User.Identity.GetUserId();
            var voteExist = _dbContext.Votes.Where(x => x.FromUser == userId && x.ToUser == toUserId).FirstOrDefault();
            if(voteExist == null)
            {
                return PartialView();
            }
            return PartialView(voteExist);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Votes votes)
        {
            AddVote(votes);
            var usrid = Url.RequestContext.RouteData.Values["id"].ToString();
            var usrDetails = _dbContext.UserDetails.Where(x => x.Id == usrid).FirstOrDefault();
            return RedirectToAction("UserProfile", "Settings", usrDetails);
        }
        
        public void AddVote(Votes vote)
        {
            var usrid = Url.RequestContext.RouteData.Values["id"].ToString();
            
            var userId = User.Identity.GetUserId();
            var voteExist = _dbContext.Votes.Where(x => x.FromUser == userId && x.ToUser == usrid).FirstOrDefault();
            if (voteExist == null)
            {
                Votes newVote = new Votes
                {
                    FromUser = userId,
                    ToUser = usrid,
                    Vote = vote.Vote
                };
                _dbContext.Votes.Add(newVote);
                _dbContext.SaveChanges();
            }
            CalculateVotesForUser(usrid);
        }

        public void CalculateVotesForUser(string id)
        {        
            var votesSum = _dbContext.Votes.Where(x => x.ToUser == id).ToList();
            float sum = 0;
            foreach (var item in votesSum)
            {
                sum += item.Vote;
            }
            var votesCount = _dbContext.Votes.Where(x => x.ToUser == id).Count();
            double avarageScore = sum / votesCount;
            var user = _dbContext.UserDetails.Where(x => x.Id == id).FirstOrDefault();
            user.Vote = avarageScore;
            _dbContext.Entry(user).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }
    }
}