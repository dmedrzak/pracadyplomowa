﻿using LanguageChat.Models;
using LanguageChat.Models.Chat;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static LanguageChat.Models.Connection;

namespace LanguageChat.Controllers
{
    public class SettingsController : Controller
    {
        private ApplicationDbContext _dbContext = new ApplicationDbContext();
        // GET: Settings
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserProfile(string id)
        {
            var userInfos = _dbContext.UserDetails.Where(x => x.Id == id).ToList().FirstOrDefault();
            
            return PartialView(userInfos);
        }


        public ActionResult UserInformation()
        {
            string userid = User.Identity.GetUserId();

            UserDetail userDetail = _dbContext.UserDetails.Where(x => x.Id == userid).FirstOrDefault();
            if(userDetail ==null)
            {
                userDetail = new UserDetail();
                userDetail.Country = "US";
            }
            return PartialView(userDetail);
        }
        [HttpPost]
        public ActionResult UserInformation(UserDetail details)
        {

            if (ModelState.IsValid)
            {
                string userid = User.Identity.GetUserId();
                UserDetail userDetail = _dbContext.UserDetails.Where(x => x.Id == userid).FirstOrDefault();
                byte[] imageData = null;
                if(Request.Files.Count >0)
                {
                    HttpPostedFileBase poImgFile = Request.Files["UserPhoto"];

                    using (var binary = new BinaryReader(poImgFile.InputStream))
                    {
                        imageData = binary.ReadBytes(poImgFile.ContentLength);
                    }
                    var user = _dbContext.Users.Where(x => x.Id == userid).SingleOrDefault();
                    user.UserPhoto = imageData;
                }


                if(userDetail == null)
                {
                    UserDetail newUserDetail = new UserDetail();
                    newUserDetail.Id= userid;
                    newUserDetail.Sex = details.Sex;
                    newUserDetail.Birthday = details.Birthday;
                    newUserDetail.Country = details.Country;
                    newUserDetail.About = details.About;
                    _dbContext.UserDetails.Add(newUserDetail);

                }
                else
                {
                    userDetail.Sex = details.Sex;
                    userDetail.Birthday = details.Birthday;
                    userDetail.Country = details.Country;
                    userDetail.About = details.About;

                    _dbContext.Entry(userDetail).State = EntityState.Modified;
                }
                _dbContext.SaveChanges();
                return RedirectToAction("UserInformation");
            }
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            return PartialView(details);
        }


        public ActionResult Languages()
        {
            var getLanguages = _dbContext.Languages.ToList();
            var languages = new LanguageViewModel { LanguageList = getLanguages };
            return View("Languages", languages);
        }

        [HttpPost]
        public ActionResult KnownLanguages(LanguageViewModel languageList)
        {
            int languageId = languageList.LanguageId;
            Language lang = _dbContext.Languages.Where(x => x.Id == languageId).FirstOrDefault();
            if (ModelState.IsValid)
            {              
                string userid = User.Identity.GetUserId();

                UserDetailLanguage userDetailLanguage = new UserDetailLanguage();
                userDetailLanguage.UserDetailId = userid;
                userDetailLanguage.LanguageId = languageId;
                userDetailLanguage.IsKnown = true;

                var ifExist = _dbContext.UserDetailLanguages.Where(x => x.LanguageId == userDetailLanguage.LanguageId).Where(x => x.UserDetailId == userDetailLanguage.UserDetailId).Where(x => x.IsKnown == userDetailLanguage.IsKnown).FirstOrDefault();
                if (ifExist !=null)
                {
                    return RedirectToAction("Languages");
                }
                else
                {
                    _dbContext.UserDetailLanguages.Add(userDetailLanguage);
                    _dbContext.SaveChanges();
                }
            }

            var knownLanguages = _dbContext.UserDetailLanguages.Where(c => c.IsKnown).Select(c => c.Language);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        public ActionResult UnknownLanguages(LanguageViewModel languageList)
        {
            int languageId = languageList.LanguageId;
            Language lang = _dbContext.Languages.Where(x => x.Id == languageId).FirstOrDefault();
            if (ModelState.IsValid)
            {
                string userid = User.Identity.GetUserId();

                UserDetailLanguage userDetailLanguage = new UserDetailLanguage();
                userDetailLanguage.UserDetailId = userid;
                userDetailLanguage.LanguageId = languageId;
                userDetailLanguage.IsKnown = false;

                var ifExist = _dbContext.UserDetailLanguages.Where(x => x.LanguageId == userDetailLanguage.LanguageId).Where(x => x.UserDetailId == userDetailLanguage.UserDetailId).Where(x =>x.IsKnown == userDetailLanguage.IsKnown).FirstOrDefault();
                if(ifExist != null)
                {
                    return RedirectToAction("Languages");
                }
                else
                {
                    _dbContext.UserDetailLanguages.Add(userDetailLanguage);
                    _dbContext.SaveChanges();
                }
            }

            var knownLanguages = _dbContext.UserDetailLanguages.Where(c => c.IsKnown).Select(c => c.Language);
            return RedirectToAction("Languages");
        }

        public ActionResult ListKnownLanguages()
        {
            string userid = User.Identity.GetUserId();
            var knownLanguages = _dbContext.UserDetailLanguages.Where(c => c.UserDetailId ==userid).Where(c => c.IsKnown).Select(c => c.Language);

            return PartialView(knownLanguages);
        }

        public ActionResult ListUnknownLanguages()
        {
            string userid = User.Identity.GetUserId();
            var unKnownLanguages = _dbContext.UserDetailLanguages.Where(c => c.UserDetailId == userid).Where(c => !c.IsKnown).Select(c => c.Language);

            return PartialView(unKnownLanguages);
        }

        public ActionResult Delete(int id)
        {
            string userid = User.Identity.GetUserId();

            var toDelete = _dbContext.UserDetailLanguages.Where(x => x.LanguageId == id).Where(x => x.UserDetailId == userid).FirstOrDefault();
            _dbContext.UserDetailLanguages.Remove(toDelete);
            _dbContext.SaveChanges();
            return RedirectToAction("Languages");
        }

        public FileContentResult UserPhotos(string id)
        {
            var userHavePhoto = _dbContext.Users.Where(x => x.Id == id).FirstOrDefault();
            string noPhoto = HttpContext.Server.MapPath(@"/Content/Images/avatar.jpg");

            if (userHavePhoto.UserPhoto == null)
            {
                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(noPhoto);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(noPhoto, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);
                return File(imageData, "image/jpg");
            }

            return new FileContentResult(userHavePhoto.UserPhoto, "image/jpeg");

        }



    }
}