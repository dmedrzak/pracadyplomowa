﻿using LanguageChat.Hubs;
using LanguageChat.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LanguageChat.Controllers
{
    public class ChatController : Controller
    {
        // GET: Chat
        public ActionResult Index(string id)
        {
            if(id !=null)
            {
                ViewBag.AddUserId = id;
            }

            return View();
        }
    }

}