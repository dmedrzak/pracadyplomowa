﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using LanguageChat.Models;
using System.Text;

namespace LanguageChat.Controllers
{
    public class TranslateController : Controller
    {
        WebClient webClient = new WebClient();
        private ApplicationDbContext _dbContext = new ApplicationDbContext();
        // GET: Translate
        public ActionResult Index()
        {
            var getLanguages = _dbContext.Languages.ToList();
            TranslateViewModel translateViwModel = new TranslateViewModel();
            translateViwModel.LanguageList = getLanguages;
            return PartialView("Index", translateViwModel);
        }

        [HttpPost]
        public ActionResult Index(TranslateViewModel model)
        {
            var getLanguages = _dbContext.Languages.ToList();
            int cnvrtFrom = Convert.ToInt32(model.fromLang);
            int cnvrtTo = Convert.ToInt32(model.toLang);
            var from = _dbContext.Languages.Where(x => x.Id == cnvrtFrom).FirstOrDefault();
            var to = _dbContext.Languages.Where(x => x.Id == cnvrtTo).FirstOrDefault();
            string langPair = from.Value.ToLower() + "|" + to.Value.ToLower();
            string text = model.textToTranslate;
            string url = String.Format(@"http://translatewebapi.azurewebsites.net/api/Translate?text={0}&langs={1}",text,langPair);
            string result;
            using (WebClient webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                 result = webClient.DownloadString(url);
            }
            TranslateViewModel translateViwModel = new TranslateViewModel();
            translateViwModel.fromLang = model.fromLang;
            translateViwModel.toLang = model.toLang;
            translateViwModel.textToTranslate = model.textToTranslate;
            translateViwModel.translatedText = result;
            translateViwModel.LanguageList = getLanguages;


            return PartialView("Translated", translateViwModel);
        }
    }
}