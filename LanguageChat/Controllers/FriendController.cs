﻿using LanguageChat.Models;
using LanguageChat.Models.Chat;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LanguageChat.Controllers
{
    public class FriendController : Controller
    {
        private ApplicationDbContext _dbContext = new ApplicationDbContext();
        // GET: Friend

       public ActionResult FriendsList()
         {
            var userId = User.Identity.GetUserId();       
            List<FriendList> myFriends = _dbContext.FriendLists.Where(x=>x.FriendOne == userId || x.FriendSecond==userId).Where(y=>y.FriendRequestOne==true &&y.FriendRequestSecond==true).ToList();
            List<string> friendsId = new List<string>();
            foreach (var item in myFriends)
            {
                if (item.FriendOne == userId)
                {
                    friendsId.Add(item.FriendSecond);
                }
                else
                    friendsId.Add(item.FriendOne);
            }
            var friendsDetails = _dbContext.Users.Where(x => friendsId.Any(y => y == x.Id)).ToList();
          return PartialView(friendsDetails);
         }

        public ActionResult RequestFriend()
        {
            var userId = User.Identity.GetUserId();
            List<string> friendsId = new List<string>();
            var requestFriends = _dbContext.FriendLists.Where(x => x.FriendOne == userId || x.FriendSecond == userId);
            var notConfirmedFriend = requestFriends.Where(x => x.FriendRequestOne == false || x.FriendRequestSecond == false);

            foreach (var item in notConfirmedFriend)
            {
                if (item.FriendSecond == userId)
                {
                    friendsId.Add(item.FriendOne);
                }
            }
            var friendsDetails = _dbContext.Users.Where(x => friendsId.Any(y => y == x.Id)).ToList();
            return PartialView(friendsDetails);
        }

        public ActionResult ConfirmFriendRequest(string id)
        {
            var userId = User.Identity.GetUserId();
            var toConfirm = _dbContext.FriendLists.Where(x=>(x.FriendOne == userId && x.FriendSecond == id) || (x.FriendOne==id && x.FriendSecond==userId)).Where(y=>y.FriendRequestOne==false || y.FriendRequestSecond==false).FirstOrDefault();

            if (toConfirm.FriendOne == userId)
            {
                toConfirm.FriendRequestOne = true;
            }
            else
                toConfirm.FriendRequestSecond = true;


            _dbContext.Entry(toConfirm).State = EntityState.Modified;
            _dbContext.SaveChanges();
            return RedirectToAction("FriendsList");
        }

        public ActionResult NonConfirmFriendRequest(string id)
        {
            var userId = User.Identity.GetUserId();
            var toConfirm = _dbContext.FriendLists.Where(x => (x.FriendOne == userId && x.FriendSecond == id) || (x.FriendOne == id && x.FriendSecond == userId)).Where(y => y.FriendRequestOne == false || y.FriendRequestSecond == false).FirstOrDefault();

            _dbContext.FriendLists.Remove(toConfirm);
            _dbContext.SaveChanges();
            return RedirectToAction("FriendsList");
        }

        public ActionResult DeleteFriend(string id)
        {
            var userId = User.Identity.GetUserId();
            var toConfirm = _dbContext.FriendLists.Where(x => (x.FriendOne == userId && x.FriendSecond == id) || (x.FriendOne == id && x.FriendSecond == userId)).FirstOrDefault();

            _dbContext.FriendLists.Remove(toConfirm);
            _dbContext.SaveChanges();
            return RedirectToAction("FriendsList");
        }

        public ActionResult Index(string id)
        {
            string fromUserId = User.Identity.GetUserId();
            var friendList = _dbContext.FriendLists.Where(x => (x.FriendOne == fromUserId && x.FriendSecond == id)|| (x.FriendOne==id && x.FriendSecond==fromUserId)).ToList();
            if(friendList.Count == 0)
            {
                AddFriend(id);
            }
            return RedirectToAction("UserProfile", "Settings",new {id = id });
        }

        public void AddFriend(string id)
        {
            string userid = User.Identity.GetUserId();

            FriendList newFriend = new FriendList
            {
                FriendOne = userid,
                FriendSecond = id,
                FriendRequestOne = true,
                FriendRequestSecond = false,
                RequestDate = DateTime.Now
            };
            _dbContext.FriendLists.Add(newFriend);
            _dbContext.SaveChanges();
        }
    }
}