﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using LanguageChat.Models;

namespace LanguageChat.Controllers
{
    public class SearchController : Controller
    {
        private ApplicationDbContext _dbContext;

        public SearchController()
        {
            _dbContext = new ApplicationDbContext(); 
        }

        // GET: Search
        public ActionResult UserSearch()
        {
            var getLanguages = _dbContext.Languages.ToList();
            UserSearchModel model = new UserSearchModel();
            model.LanguageList = getLanguages;
            var viewModel = new UserSearchModel { LanguageList = getLanguages };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult UserSearch(UserSearchModel searchModel)
        {
            var res = userSearchModel(searchModel).ToList();

            return PartialView("SearchResult",res);
        }


        public List<UserDetail> userSearchModel(UserSearchModel searchModel)
        {
            string userid = User.Identity.GetUserId();
            var user = _dbContext.UserDetails.Where(x => x.Id == userid);
            var result = _dbContext.UserDetails.Except(user).ToList().AsQueryable();
            List<Connection> usersFound = new List<Connection>();         
            if (searchModel != null)
            {

                if (!String.IsNullOrEmpty(searchModel.Id))
                    result = result.Where(x => x.Id == searchModel.Id);
                if (searchModel.Male && searchModel.Female)
                {
                    result = result.Where(x => x.Sex == "Male" || x.Sex == "Female");
                }else
                if (searchModel.Male)
                {
                    result = result.Where(x => x.Sex == "Male");
                }else
                if (searchModel.Female)
                {
                    result = result.Where(x => x.Sex == "Female");
                }
          
                if (searchModel.AgeFrom.HasValue)
                    result = result.Where(x => DateTime.Now.Year - x.Birthday.Year >= searchModel.AgeFrom);
                if (searchModel.AgeTo.HasValue)
                    result = result.Where(x => DateTime.Now.Year - x.Birthday.Year <= searchModel.AgeTo);

                if (!searchModel.LanguageId.All(item => item == 0))
                {
                    List<UserDetailLanguage> usrDetails = new List<UserDetailLanguage>();
                    foreach (var item in searchModel.LanguageId)
                    {
                        var details = _dbContext.UserDetailLanguages.Where(x => x.LanguageId == item).ToList();
                        foreach (var item2 in details)
                        {
                            usrDetails.Add(item2);
                        }
                    }
                    result = result.Where(x => usrDetails.Intersect(x.UserDetailLanguages).Count() > 0);
                }

            }
            
            return result.ToList();
        }
    }
}